const { body, param, check } = require("express-validator");
const messageConfig = require("../config/message.json");

module.exports = (method) => {
  switch (method) {
    case "signupUser":
      return [
        body("UserName", messageConfig.UserNameRequired.Message)
          .trim()
          .notEmpty(),
        body("Email", messageConfig.EmailRequired.Message)
          .trim()
          .notEmpty()
          .isEmail()
          .withMessage(messageConfig.InvalidEmailFormat.Message),
        body("Password", messageConfig.PasswordRequired.Message)
          .trim()
          .notEmpty()
          .matches("^[a-zA-Z0-9!@#$%^&*]{8,}$")
          .withMessage(messageConfig.PasswordFormatError.Message),
      ];
    case "loginUser":
      return [
        body("Email", messageConfig.EmailRequired.Message).trim().notEmpty(),

        body("Password", messageConfig.PasswordRequired.Message)
          .trim()
          .notEmpty(),
      ];
    case "addMenu":
      return [
        body("Name", messageConfig.MenuNameRequired.Message).trim().notEmpty(),

        body("Description", messageConfig.MenuDescriptionRequired.Message)
          .trim()
          .notEmpty(),
      ];
    case "updateMenu":
      return [
        param("Id", messageConfig.MenuIdRequired.Message).trim().notEmpty(),
        body("Name", messageConfig.MenuNameRequired.Message).trim().notEmpty(),

        body("Description", messageConfig.MenuDescriptionRequired.Message)
          .trim()
          .notEmpty(),
      ];
    case "addRestaurant":
      return [
        body("Name", messageConfig.RestaurantNameRequired.Message)
          .trim()
          .notEmpty(),

        body("Description", messageConfig.RestaurantDescriptionRequired.Message)
          .trim()
          .notEmpty(),
        body("Menu", messageConfig.RestaurantMenuRequired.Message)
          .trim()
          .notEmpty(),
      ];
    case "updateRestaurant":
      return [
        param("Id", messageConfig.RestaurantIdRequired.Message)
          .trim()
          .notEmpty(),
        body("Name", messageConfig.RestaurantNameRequired.Message)
          .trim()
          .notEmpty(),

        body("Description", messageConfig.RestaurantDescriptionRequired.Message)
          .trim()
          .notEmpty(),
        body("Menu", messageConfig.RestaurantMenuRequired.Message)
          .trim()
          .notEmpty(),
      ];

    default:
      break;
  }
};
