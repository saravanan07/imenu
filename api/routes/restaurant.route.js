const express = require("express");
const router = express.Router();
const validate = require("../middleware/validate");
const checkAuth = require("../middleware/checkAuth").checkAuth;

const restaurantController = require("../controller/restaurant.controller");

router.post(
  "/add-restaurant",
  checkAuth,
  validate("addRestaurant"),
  restaurantController.addRestaurant
);
router.get(
  "/list-restaurants",
  checkAuth,
  restaurantController.listAllRestaurants
);
router.put(
  "/update-restaurant/:Id",
  checkAuth,
  validate("updateRestaurant"),
  restaurantController.updateRestaurant
);
router.delete(
  "/delete-restaurant/:Id",
  checkAuth,
  restaurantController.deleteRestaurant
);

module.exports = router;
