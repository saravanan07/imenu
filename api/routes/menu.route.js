const express = require("express");
const router = express.Router();
const validate = require("../middleware/validate");
const checkAuth = require("../middleware/checkAuth").checkAuth;

const menuController = require("../controller/menu.controller");

router.post(
  "/add-menu",
  checkAuth,
  validate("addMenu"),
  menuController.addMenu
);
router.get("/list-menus", checkAuth, menuController.listAllMenus);
router.put(
  "/update-menu/:Id",
  checkAuth,
  validate("updateMenu"),
  menuController.updateMenu
);
router.delete("/delete-menu/:Id", checkAuth, menuController.deleteMenu);

module.exports = router;
