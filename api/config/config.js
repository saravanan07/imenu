module.exports = {
  AccessTokenExpiry: process.env.AccessTokenExpiry,
  ConnectionString: process.env.ConnectionString,
  JwtKey: process.env.JwtKey,
  JwtRefreshKey: process.env.JwtRefreshKey,
  RefreshTokenExpiry: process.env.RefreshTokenExpiry,
  Port: process.env.Port,
  CookieExpiry: process.env.CookieExpiry,
};
