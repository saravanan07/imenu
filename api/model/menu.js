const mongoose = require("mongoose");

const menuSchema = mongoose.Schema({
  Name: {
    type: String,
    required: true,
  },
  Description: {
    type: String,
  },
  Items: [], // {Name:String,Description:String,Image:String}
  CreatedOn: { type: Date },
  UpdatedOn: { type: Date },
});

const menu = mongoose.model("Menu", menuSchema);
module.exports = menu;
