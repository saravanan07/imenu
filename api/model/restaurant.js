const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const restaurantSchema = mongoose.Schema({
  Name: {
    type: String,
    required: true,
  },
  Description: {
    type: String,
  },
  Type: {
    type: String,
  },
  Menu: { type: Schema.Types.ObjectId, ref: "Menu" }, // Menu Id (String)

  CreatedOn: { type: Date },
  UpdatedOn: { type: Date },
});

const restaurant = mongoose.model("Restaurant", restaurantSchema);
module.exports = restaurant;
