const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const config = require("../config/config");

const userSchema = mongoose.Schema({
  UserName: {
    type: String,
    required: true,
  },
  Email: {
    type: String,
    required: true,
    unique: true,
    match:
      /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
  },
  Password: {
    type: String,
  },
  CreatedOn: { type: Date },
  UpdatedOn: { type: Date },
});

userSchema.methods.setPassword = (password) => {
  // console.log(password)
  const salt = bcrypt.genSaltSync(10);
  return bcrypt.hashSync(password, salt);
};

userSchema.methods.isValidPassword = (password, hashedPassword) => {
  return bcrypt.compareSync(password, hashedPassword);
};

//generate acess token
userSchema.methods.generateJWT = (user) => {
  return jwt.sign(
    {
      _id: user._id,
      UserName: user.UserName,
      Email: user.Email,
      CreatedOn: user.CreatedOn,
    },
    config.JwtKey,
    { expiresIn: config.AccessTokenExpiry }
  );
};

userSchema.pre("save", function (next) {
  // Auto-update of CreatedOn && UpdatedOn field when saving a document
  var currentDate = new Date();
  this.UpdatedOn = currentDate;
  if (!this.CreatedOn) this.CreatedOn = currentDate;

  next();
});

const user = mongoose.model("User", userSchema);

module.exports = user;
