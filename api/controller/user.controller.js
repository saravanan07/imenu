const { validationResult, query } = require("express-validator");
const nodemailer = require("nodemailer");

const User = require("../model/user");

const config = require("../config/config");
const messageConfig = require("../config/message.json");

RegExp.escape = function (text) {
  text = text.toString();
  return text.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
};

/**
 * Create a new user (User registration)
 *
 * @param {String} UserName - Username of the user.
 * @param {String} Email - Email of the user.
 * @param {String} Password - Password of the user.
 * @return {String} - Success/Failure message.
 */
exports.signupUser = (req, res, next) => {
  const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
  if (!errors.isEmpty()) {
    return res.status(400).json({
      Message: errors
        .array({ onlyFirstError: true })
        .map((x) => x.msg)
        .toString(),
    });
  }

  const body = req.body;
  var userData = {
    UserName: body.UserName,
    Email: body.Email,
    IsActive: false,
  };

  var email = new RegExp(["^", RegExp.escape(body.Email), "$"].join(""), "i");
  User.findOne({ Email: email })
    .exec()
    .then((user) => {
      if (user) {
        return res
          .status(409)
          .json({ Message: messageConfig.UserAlreadyExists.Message });
      } else {
        var userInfo = new User(userData);
        userInfo.Password = userInfo.setPassword(body.Password);

        userInfo
          .save()
          .then((newUser) => {
            res.status(201).json({
              Message: messageConfig.UserSavedSuccess.Message,
              User: newUser,
            });
          })
          .catch((err) => {
            next(err);
            res.status(500).json({
              Message: messageConfig.ServerError.Message,
            });
          });
      }
    })
    .catch((err) => {
      next(err);
      res.status(500).json({ Message: messageConfig.ServerError.Message });
    });
};

/**
 * User Login
 *
 * @param {String} Email - Email of the user.
 * @param {String} Password - Password of the user.
 * @return {String} - Success/Failure message.
 */
exports.loginUser = (req, res, next) => {
  const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
  if (!errors.isEmpty()) {
    return res.status(400).json({
      Message: errors
        .array({ onlyFirstError: true })
        .map((x) => x.msg)
        .toString(),
    });
  }

  const body = req.body;

  var email = new RegExp(["^", RegExp.escape(body.Email), "$"].join(""), "i");
  User.findOne({ Email: email })
    .exec()
    .then((user) => {
      if (!user) {
        return res
          .status(401)
          .json({ Message: messageConfig.AuthenticationFailed.Message });
      }
      if (user.Password && user.isValidPassword(body.Password, user.Password)) {
        const accessToken = user.generateJWT(user);
        console.log(accessToken);
        return res.status(200).json({
          Message: messageConfig.AuthenticationSuccess.Message,
          Token: accessToken,
        });
      } else {
        res.status(401).json({
          Message: messageConfig.AuthenticationFailed.Message,
        });
      }
    })
    .catch((err) => {
      next(err);
      res.status(500).json({ Message: messageConfig.ServerError.Message });
    });
};
