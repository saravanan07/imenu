const Menu = require("../model/menu");
const { validationResult, query } = require("express-validator");
const messageConfig = require("../config/message.json");

/**
 * Add a new menu
 *
 * @body {String} Name - Name of the Menu.
 * @body {String} Description - Description of the menu.
 * @body {Array} Items - Items of the menu.
 * @return {String} - Success/Failure message.
 */
exports.addMenu = (req, res, next) => {
  const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
  if (!errors.isEmpty()) {
    return res.status(400).json({
      Message: errors
        .array({ onlyFirstError: true })
        .map((x) => x.msg)
        .toString(),
    });
  }

  const { Name, Description, Items } = req.body;

  let menu = new Menu({
    Name,
    Description,
    Items,
  });
  menu
    .save()
    .then((newMenu) => {
      return res.status(201).json({
        Message: messageConfig.MenuAdded.Message,
        Menu: newMenu,
      });
    })
    .catch((err) => {
      next(err);
      res.status(500).json({
        Message: messageConfig.ServerError.Message,
      });
    });
};

/**
 * Update a menu
 *
 * @param {String} Id - Id of the Menu.
 * @body {String} Name - Name of the Menu.
 * @body {String} Description - Description of the menu.
 * @body {Array} Items - Items of the menu.
 * @return {String} - Success/Failure message.
 */
exports.updateMenu = (req, res, next) => {
  const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
  if (!errors.isEmpty()) {
    return res.status(400).json({
      Message: errors
        .array({ onlyFirstError: true })
        .map((x) => x.msg)
        .toString(),
    });
  }
  const menuId = req.params.Id;
  const { Name, Description, Items } = req.body;

  Menu.findById(menuId)
    .exec()
    .then((menu) => {
      if (menu) {
        menu.Name = Name;
        menu.Description = Description;
        menu.Items = Items;

        menu
          .save()
          .then((updatedMenu) => {
            res.status(201).json({
              Message: messageConfig.MenuUpdated.Message,
              Menu: updatedMenu,
            });
          })
          .catch((err) => {
            next(err);
            res.status(500).json({
              Message: messageConfig.ServerError.Message,
            });
          });
      } else {
        res.status(400).json({ Message: messageConfig.MenuNotFound.Message });
      }
    })
    .catch((err) => {
      next(err);
      res.status(500).json({ Message: messageConfig.ServerError.Message });
    });
};

/**
 * Delete a menu
 *
 * @param {String} Id - Id of the Menu.
 *
 * @return {String} - Success/Failure message.
 */
exports.deleteMenu = (req, res, next) => {
  const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
  if (!errors.isEmpty()) {
    return res.status(400).json({
      Message: errors
        .array({ onlyFirstError: true })
        .map((x) => x.msg)
        .toString(),
    });
  }
  const menuId = req.params.Id;

  Menu.findByIdAndDelete(menuId)
    .exec()
    .then(() => {
      res.status(201).json({
        Message: messageConfig.MenuDeleted.Message,
      });
    })
    .catch((err) => {
      next(err);
      res.status(500).json({ Message: messageConfig.ServerError.Message });
    });
};

/**
 * List all menus
 *
 *
 * @return {String} - Success/Failure message.
 */
exports.listAllMenus = (req, res, next) => {
  Menu.find()
    .exec()
    .then((menu) => {
      res.status(201).json({
        Menu: menu,
      });
    })
    .catch((err) => {
      next(err);
      res.status(500).json({ Message: messageConfig.ServerError.Message });
    });
};
