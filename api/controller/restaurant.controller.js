const Restaurant = require("../model/restaurant");
const { validationResult, query } = require("express-validator");
const messageConfig = require("../config/message.json");

/**
 * Add a new Restaurant
 *
 * @body {String} Name - Name of the Restaurant.
 * @body {String} Description - Description of the Restaurant.
 * @body {String} Menu - Menu Id of the Restaurant.
 * @return {String} - Success/Failure message.
 */
exports.addRestaurant = (req, res, next) => {
  const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
  if (!errors.isEmpty()) {
    return res.status(400).json({
      Message: errors
        .array({ onlyFirstError: true })
        .map((x) => x.msg)
        .toString(),
    });
  }

  const { Name, Description, Menu } = req.body;

  let restaurant = new Restaurant({
    Name,
    Description,
    Menu,
  });
  restaurant
    .save()
    .then((newRestaurant) => {
      return res.status(201).json({
        Message: messageConfig.RestaurantAdded.Message,
        Restaurant: newRestaurant,
      });
    })
    .catch((err) => {
      next(err);
      res.status(500).json({
        Message: messageConfig.ServerError.Message,
      });
    });
};

/**
 * Update a restaurant
 *
 * @param {String} Id - Id of the restaurant.
 * @body {String} Name - Name of the restaurant.
 * @body {String} Description - Description of the restaurant.
 * @body {String} Menu - Menu id of the restaurant.
 * @return {String} - Success/Failure message.
 */
exports.updateRestaurant = (req, res, next) => {
  const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
  if (!errors.isEmpty()) {
    return res.status(400).json({
      Message: errors
        .array({ onlyFirstError: true })
        .map((x) => x.msg)
        .toString(),
    });
  }
  const restaurantId = req.params.Id;
  const { Name, Description, Menu } = req.body;

  Restaurant.findById(restaurantId)
    .exec()
    .then((restaurant) => {
      if (restaurant) {
        restaurant.Name = Name;
        restaurant.Description = Description;
        restaurant.Menu = Menu;

        restaurant
          .save()
          .then((updatedRestaurant) => {
            res.status(201).json({
              Message: messageConfig.RestaurantAdded.Message,
              Restaurant: updatedRestaurant,
            });
          })
          .catch((err) => {
            next(err);
            res.status(500).json({
              Message: messageConfig.ServerError.Message,
            });
          });
      } else {
        res
          .status(400)
          .json({ Message: messageConfig.RestaurantNotFound.Message });
      }
    })
    .catch((err) => {
      next(err);
      res.status(500).json({ Message: messageConfig.ServerError.Message });
    });
};

/**
 * Delete a restaurant
 *
 * @param {String} Id - Id of the Mrestaurantenu.
 *
 * @return {String} - Success/Failure message.
 */
exports.deleteRestaurant = (req, res, next) => {
  const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
  if (!errors.isEmpty()) {
    return res.status(400).json({
      Message: errors
        .array({ onlyFirstError: true })
        .map((x) => x.msg)
        .toString(),
    });
  }
  const restaurantId = req.params.Id;

  Restaurant.findByIdAndDelete(restaurantId)
    .exec()
    .then(() => {
      res.status(201).json({
        Message: messageConfig.RestaurantDeleted.Message,
      });
    })
    .catch((err) => {
      next(err);
      res.status(500).json({ Message: messageConfig.ServerError.Message });
    });
};

/**
 * List all restaurants
 *
 *
 * @return {String} - Success/Failure message.
 */
exports.listAllRestaurants = (req, res, next) => {
  Restaurant.find()
    .populate("Menu")
    .exec()
    .then((restaurants) => {
      res.status(201).json({
        Restaurants: restaurants,
      });
    })
    .catch((err) => {
      next(err);
      res.status(500).json({ Message: messageConfig.ServerError.Message });
    });
};
