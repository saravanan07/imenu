global.__base = __dirname + "/";
const path = require("path");
require("dotenv").config({
  path: path.join(__dirname, ".env"),
});
const http = require("http");
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const messageConfig = require("./api/config/message.json");

const userRoutes = require("./api/routes/user.route");
const menuRoutes = require("./api/routes/menu.route");
const restaurantRoutes = require("./api/routes/restaurant.route");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ limit: "100mb" }));
app.use(cookieParser());
// Allow cors everywhere
app.use(cors());
app.use("/user", userRoutes);
app.use("/menu", menuRoutes);
app.use("/restaurant", restaurantRoutes);

const port = process.env.PORT || 5000;
const server = http.createServer(app);
server.listen(port, () => {
  console.log(`Api running on localhost: ${port}`);
});

const mongoose = require("mongoose");
mongoose.connect(process.env.ConnectionString, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
});
const dbConnection = mongoose.connection;

dbConnection.on("error", function () {
  console.log(messageConfig.DbConnectionFailed.Message);
});

dbConnection.once("open", function () {
  console.log(messageConfig.DbConnectionSuccess.Message);
});
